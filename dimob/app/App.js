import React from 'react';
import { Icon } from 'react-native-elements';
import { Button, Text, View, StyleSheet, ActivityIndicator, FlatList } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Jobs, JobsScreen, JobDetails } from './screens/Jobs'
import { Rosters, RosterScreen, RosterDetails } from './screens/Roster'
import { Login, HomeScreen } from './screens/Login'
import { Profile } from './screens/Profile'
import { Settings } from './screens/Settings'


const HomeStack = createStackNavigator({
  Home: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Details: HomeScreen,
});

const JobStack = createStackNavigator({
  Jobs: {
    screen: JobsScreen,
    navigationOptions: {
      header: null
    }
  },
  Details: Jobs,
  Info: JobDetails
});

const RosterStack = createStackNavigator({
  Roster: {
    screen: RosterScreen,
    navigationOptions: {
      header: null
    }
  },
  RosterList: Rosters,
  Info: RosterDetails
});

const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile,
    navigationOptions: {
      header: null
    }
  },
});

const SettingsStack = createStackNavigator({
  Settings: {
    screen: Settings,
    navigationOptions: {
      header: null
    }
  },
});

function getIconName(routeName, focused) {
  switch (routeName) {
    case 'Home':
      return `md-home${focused ? '' : ''}`;
    case 'Settings':
      return `md-settings${focused ? '' : ''}`;
    case 'Profile':
      return `md-person${focused ? '' : ''}`;
    case 'Jobs':
      return `md-briefcase${focused ? '' : ''}`;
      case 'Roster':
        return `md-calendar${focused ? '' : ''}`;
  }
}

export default createAppContainer(createBottomTabNavigator(
  {
    Home: { screen: HomeStack },
    Jobs: { screen: JobStack },
    Roster: { screen: RosterStack },
    Profile: { screen: ProfileStack },
    Settings: { screen: SettingsStack },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        const iconName = getIconName(routeName, focused);
        return <Icon name={iconName} type="ionicon" size={28} color={tintColor} />;
      },
      header: null
    }),
    tabBarOptions: {
      activeTintColor: '#6666FF',
      inactiveTintColor: 'gray',
    },
  }
));


const styles = StyleSheet.create({
  workerJobs: {
    height: 100,
    fontSize: 20,
    borderTopWidth: 1,
    padding: 5
  }
})