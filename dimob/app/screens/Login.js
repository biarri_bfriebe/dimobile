import React, { Component } from 'react';
import { decode as atob, encode as btoa } from 'base-64'
import {
  FlatList, ActivityIndicator, Text, View,
  TouchableOpacity, StyleSheet, TextInput, Button, Image, Alert
} from 'react-native';

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.infoLogo}>
          <View style={{ flex: 2 }}>
            <Image source={require('../assets/butterfly.png')}
              style={styles.logo} />
          </View>
          <View style={{ flex: 1 }}>
            <Text style={styles.infoText}>
              Welcome to <Text style={{ fontWeight: 'bold' }}>DIMO</Text> Mobile.</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={styles.infoText}> Enter your <Text style={{ fontStyle: 'italic' }}>Email</Text> and <Text style={{ fontStyle: 'italic' }}>Password</Text> below to continue.</Text>
          </View>
        </View>
        <View style={styles.content}>
          <TextInput
            style={styles.inputContainer}
            placeholder="Email"
          />
          <TextInput
            secureTextEntry={true}
            password={true}
            style={styles.inputContainer}
            placeholder="Password"
          />
        </View>
        <View style={styles.actionBar}>
          <Text style={{ color: '#FFFFFF' }}>
            Forgot Password?
          </Text>
          <TouchableOpacity style={styles.loginButton} onPress={() => {
            Alert.alert("Login", 'Successfully Logged in!');
            this.props.navigation.navigate('Details');
          }}>
            <Text style={{ color: '#555555', fontSize: 16 }}>Log In</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

// export class HomeScreen extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//         {/* other code from before here */}
//         <Button
//           title="Go to Details"
//           onPress={() => this.props.navigation.navigate('Details')}
//         />
//       </View>
//     );
//   }
// }


export class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    // https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/scheduler_allocations/?worker=9267&ordering=start_time&start_time__gte=2019-10-14+21:00:00&limit=3&format=json
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let dateTime = date + '+' + time;
    let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/scheduler_allocations/?worker=9267&ordering=start_time&start_time__gte=' + dateTime + '&limit=4&format=json'
    return fetch(jobUrl, {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, padding: 0 }}>
        <View style={styles.currentWork}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Info', { item: this.state.dataSource.results[0] })}>
            <View>
              <Text style={styles.jobInfoTitle}>
                Current Job
              </Text>
            </View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={styles.jobInfoCube}>
                <Text style={styles.rowText}>Job ID: {this.state.dataSource.results[0].id}</Text>
                <Text style={styles.rowText}>Type: {this.state.dataSource.results[0].activity_type_name}</Text>
                <Text style={styles.rowText}>Status: {this.state.dataSource.results[0].completed ? "Complete" : "Pending"}</Text>
                <Text style={styles.rowText}>Date: {this.state.dataSource.results[0].start_date}</Text>
              </View>
              <View style={styles.jobInfoCube}>
                <Text style={styles.rowText}>Start Time: {this.state.dataSource.results[0].start_time.substring(11, 16)}</Text>
                <Text style={styles.rowText}>Location: {this.state.dataSource.results[0].location_name}</Text>
                <Text style={styles.rowText}>Priority: {this.state.dataSource.results[0].activity_priority_name}</Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={styles.infoSquare}>
            <Text style={styles.infoSquareTitle}>
              Pending Jobs
            </Text>
            <Text style={styles.infoSquareNumber}>
              3
            </Text>
          </View>
          <View style={styles.infoSquare}>
            <Text style={styles.infoSquareTitle}>
              Complete Jobs
            </Text>
            <Text style={styles.infoSquareNumber}>
              2
            </Text>
          </View>
        </View>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={styles.infoSquare}>
            <Text style={styles.infoSquareTitle}>
              Hours Worked
            </Text>
            <Text style={styles.infoSquareNumber}>
              04:34
            </Text>
          </View>
          <View style={styles.infoSquare}>
            <Text style={styles.infoSquareTitle}>
              Quota
            </Text>
            <Text style={styles.infoSquareNumber}>
              15
            </Text>
          </View>
        </View>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between'
  },
  infoLogo: {
    flex: 2,
    justifyContent: 'space-between',
    alignItems: 'center',
    fontSize: 20
  },
  infoText: {
    textAlign: 'center',
    fontSize: 20,
    color: '#555555',
    fontWeight: "500",
    fontFamily: 'sans-serif'
  },
  content: {
    flex: 1,
    padding: 20,
  },
  actionBar: {
    backgroundColor: '#555555',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 12,
    height: 70,
  },
  inputContainer: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 5,
    borderColor: 'lightgrey',
    backgroundColor: 'white',
    fontFamily: 'sans-serif',
    height: 50,
    paddingLeft: 15,
    fontSize: 20,
    margin: 6
  },
  loginButton: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    width: 80,
    borderRadius: 5
  },
  logo: {
    margin: 20,
    width: 120,
    height: 120,
    resizeMode: 'contain'
  },
  infoSquare: {
    backgroundColor: '#00bfff',
    flex: 1,
    margin: 2
  },
  currentWork: {
    backgroundColor: "#88bb88",
    flex: 1,
    margin: 2
  },
  rowText: {
    padding: 2,
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ffffff'
  },
  jobInfoTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    padding: 5,
    color: "#ffffff",
    textAlign: 'center'
  },
  jobInfoCube: {
    flex: 1,
    padding: 5,
    alignItems: 'center'
  },
  infoSquareTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ffffff',
    padding: 5,
    textAlign: 'center'
  },
  infoSquareNumber: {
    fontSize: 60,
    fontWeight: 'bold',
    color: '#ffffff',
    padding: 5,
    paddingTop: 15,
    textAlign: 'center'
  },
});

