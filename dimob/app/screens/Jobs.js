import React, { Component } from 'react';
import { decode as atob, encode as btoa } from 'base-64'
import {
  Button, Text, View, StyleSheet, ActivityIndicator,
  TouchableOpacity, FlatList, TouchableWithoutFeedback, Alert, RefreshControl
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';


function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}


export class Jobs extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    return fetch('https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/scheduler_allocations/?worker=9267&ordering=start_time&format=json', {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, padding: 0 }}>
        <FlatList
          data={this.state.dataSource}
          renderItem={({ item }) =>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Info', { item: item })}>
              <View style={[styles.workerJobsAll, { backgroundColor: item.completed ? "#88bb88" : "#bb8888" }]}>
                <Text style={styles.rowText}>Job ID: {item.id}</Text>
                <Text style={styles.rowText}>Type: {item.activity_type_name}</Text>
                <Text style={styles.rowText}>Status: {item.completed ? "Complete" : "Pending"}</Text>
                <Text style={styles.rowText}>Date: {item.start_date}</Text>
              </View>
            </TouchableOpacity>}
          keyExtractor={({ id }, index) => id}

        />
      </View>
    );
  }
}

export class JobsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let dateTime = date + '+' + time;
    let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/scheduler_allocations/?worker=9267&ordering=start_time&start_time__gte=' + dateTime + '&limit=4&format=json&completed=false';
    const params = {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return fetch(jobUrl, params)
      .then((response) => response.json())
      .then((responseJson) => {

        fetch('https://dimo-testing.biarriworkbench.com/dimo/internal/locations/', params)
          .then(res => res.json())
          .then(res => {

            this.setState({
              isLoading: false,
              dataSource: responseJson,
              data: {
                ...this.state.data,
                locations: res
              }
            });
          })


      })
      .catch((error) => {
        console.error(error);
      });
  }



 


  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'stretch' }}>
        <View style={styles.header}>
          <Text style={styles.headerText}>Upcoming Jobs</Text>
        </View>
        <View style={{ flex: 1, padding: 0, alignItems: 'stretch' }}>
          <FlatList
            data={this.state.dataSource.results}
            renderItem={({ item }) =>
              <TouchableOpacity style={styles.gridRow} onPress={() => this.props.navigation.navigate('Info', { item, location: this.state.data.locations })}>
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: item.completed ? "#aaddaa" : "#00bfff" }}>
                  <View style={styles.workerJobs}>
                    <Text style={styles.rowText}>Job ID: {item.id}</Text>
                    <Text style={styles.rowText}>Type: {item.activity_type_name}</Text>
                    <Text style={styles.rowText}>Status: {item.completed ? "Complete" : "Pending"}</Text>
                    <Text style={styles.rowText}>Date: {item.start_date}</Text>
                    <Text style={styles.rowText}>Start Time: {item.start_time.substring(11, 16)}</Text>
                    <Text style={styles.rowText}>Location: {item.location_name}</Text>
                    <Text style={styles.rowText}>Priority: {item.activity_priority_name}</Text>
                  </View>
                  <View style={styles.buttonColumn}>
                    <TouchableOpacity style={styles.jobButton} onPress={() => {
                      Alert.alert("Job Status", 'Job Started!');
                      // this.props.navigation.navigate('Details');
                    }}>
                      <Text style={{ color: '#555555', fontSize: 16 }}>Start</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.jobButton} onPress={() => {
                      Alert.alert("Job Status", 'Job Stopped!');
                      // this.props.navigation.navigate('Details');
                    }}>
                      <Text style={{ color: '#555555', fontSize: 16 }}>Stop</Text>
                    </TouchableOpacity>
                    <Text style={{ color: 'white', paddingTop: 8, fontWeight: 'bold' }}>
                      Allocated Time: {item.allocation_duration}
                    </Text>

                  </View>
                </View>
              </TouchableOpacity>}
            keyExtractor={({ id }, index) => id}
          />

        </View>
        <View style={{ flexDirection: "row"}}>
          <View style={{ flex: 1 }}>
          <TouchableOpacity style={styles.allButton} onPress={() => {
            // Alert.alert("Job Status", 'Job Started!');
            this.componentDidMount();
          }}>
            <Text style={{ color: '#ffffff', fontSize: 16 }}>Refresh</Text>
          </TouchableOpacity>
          </View>
          <View style={{ flex: 1 }}>
          <TouchableOpacity style={styles.allButton} onPress={() => {
            // Alert.alert("Job Status", 'Job Started!');
            this.props.navigation.navigate('Details');
          }}>
            <Text style={{ color: '#ffffff', fontSize: 16 }}>View all jobs</Text>
          </TouchableOpacity>
          </View>
          
        </View>
      </View>
    );
  }
}

export class JobDetails extends React.Component {



  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    let data = this.props.navigation.getParam('item', 'Failed to load job');
    const locationUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/locations/?name=' + data.location_name
    return fetch(locationUrl, {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }


    let data = this.props.navigation.getParam('item', 'Failed to load job');
    let location = this.state.dataSource[0]
    let mapChoice = Math.floor(Math.random() * 5);
    const viewport = { latitudeDelta: 0.00922, longitudeDelta: 0.00421 }
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'stretch' }}>
        <View>
          <MapView initialRegion={{latitude: location.latitude, longitude: location.longitude, ...viewport}} provider={PROVIDER_GOOGLE} style={styles.map}>
            <MapView.Marker
              coordinate={{ latitude: location.latitude, longitude: location.longitude }}
              //  title="Job Location"
              title='172 Robertson St, Fortitude Valley'
            />
          </MapView>
        </View>
        <View style={{ flex: 1, alignItems: "stretch" }}>
          <View style={styles.jobInfoHeader}>
            <Text style={styles.jobInfoTitle}>Job ID: {data.id}</Text>
          </View>
          <View>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Type:</Text> {data.activity_type_name}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Status:</Text> {data.completed ? "Complete" : "Pending"}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Date:</Text> {data.start_date}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Start Time:</Text> {data.start_time.substring(11, 16)}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Location:</Text> {data.location_name}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Priority:</Text> {data.activity_priority_name}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Created By:</Text> {data.source_data.CREATED_BY}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  workerJobs: {
    // backgroundColor: '#bb8888',
    height: 160,
    padding: 5,
    paddingLeft: 20,
    flex: 1
  },
  workerJobsAll: {
    backgroundColor: '#bb8888',
    height: 100,
    borderTopWidth: 1,
    borderColor: 'white',
    padding: 5,
    paddingLeft: 20
  },
  rowText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#ffffff'
  },
  header: {
    alignItems: 'center',
    backgroundColor: "#555555",
    padding: 10
  },
  headerText: {
    fontSize: 20,
    color: "#ffffff"
  },
  allButton: {
    backgroundColor: '#888888',
    color: "#ffffff",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    borderRadius: 2,
    margin: 2
  },
  jobButton: {
    backgroundColor: '#ffffff',
    color: "#000000",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    marginTop: 10,
    width: 150,
    borderRadius: 2
  },
  buttonColumn: {
    padding: 10,
    alignItems: 'center'
  },
  gridRow: {
    borderTopWidth: 1,
    borderColor: 'white',
  },
  map: {
    // flex: 1,
    width: 500,
    height: 250
  },
  jobInfo: {
    fontSize: 20,
    padding: 5,
  },
  jobInfoTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    padding: 5,
    color: "#ffffff"
  },
  jobInfoHeader: {
    backgroundColor: "#555555",
  }
})
