import React, { Component } from 'react';
import { decode as atob, encode as btoa } from 'base-64'
import {
  Button, Text, View, StyleSheet, ActivityIndicator,
  TouchableOpacity, FlatList, TouchableWithoutFeedback, Alert, RefreshControl
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';


function wait(timeout) {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}


function dayName(dayInt) {
  switch (dayInt) {
    case 0:
      return `Sunday`;
    case 1:
      return `Monday`;
    case 2:
      return `Tuesday`;
    case 3:
      return `Wednesday`; 
    case 4:
      return `Thursday`;
    case 5:
      return `Friday`;
    case 6:
      return `Saturday`;  
  }
}

Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

export class RosterScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let dateTime = date + '+' + time;
    // let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/availability_relationships/?worker=9267&ordering=date&date__gte=2019-11-12';
    let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/availability_relationships/?worker=9267&ordering=date&date__gte=' + date + '&limit=7&format=json';
    const params = {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return fetch(jobUrl, params)
      .then((response) => response.json())
      .then((responseJson) => {

        fetch('https://dimo-testing.biarriworkbench.com/dimo/internal/availability_types/', params)
          .then(res => res.json())
          .then(res => {

            this.setState({
              isLoading: false,
              dataSource: responseJson,
              data: {
                ...this.state.data,
                availability_types: res
              }
            });
          })


      })
      .catch((error) => {
        console.error(error);
      });
  }



  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'stretch' }}>
        <View style={styles.header}>
          <Text style={styles.headerText}>My Roster</Text>
        </View>
        <View style={{ flex: 1, padding: 0, alignItems: 'stretch' }}>
          <FlatList
            data={this.state.dataSource.results}
            renderItem={({ item }) => {
              const availability = this.state.data.availability_types.filter(obj => obj.id===item.availability_reason)[0];
              const dt = new Date(item.date)
              const day = dt.getDay()
              return (
                <TouchableOpacity style={styles.gridRow} onPress={() => this.props.navigation.navigate('Info', { item, availability })}>
                  <View style={styles.dateHeader}>
                    <Text style ={{color: '#ffffff'}}>{dayName(day)} - {item.date}</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', backgroundColor: availability.color }}>
                    <View style={styles.workerJobs}>
                    <Text style={styles.rowText}>Rostered: {availability.name}</Text>
                    <Text style={styles.rowText}>Code: {availability.short_display_name}</Text>
                      <Text style={styles.rowText}>Description: {availability.description}</Text>
                      <Text style={styles.rowText}>Start Time: {availability.start_time}</Text>
                      <Text style={styles.rowText}>End Time: {availability.end_time}</Text>
                    </View>
                    
                  </View>
                </TouchableOpacity>
              );
            }}
            keyExtractor={({ id }, index) => id}
          />

        </View>
        <View style={{ flexDirection: "row"}}>
          <View style={{ flex: 1 }}>
          <TouchableOpacity style={styles.allButton} onPress={() => {
            // Alert.alert("Job Status", 'Job Started!');
            this.componentDidMount();
          }}>
            <Text style={{ color: '#ffffff', fontSize: 16 }}>Refresh</Text>
          </TouchableOpacity>
          </View>
          <View style={{ flex: 1 }}>
          <TouchableOpacity style={styles.allButton} onPress={() => {
            this.props.navigation.navigate('RosterList');
          }}>
            <Text style={{ color: '#ffffff', fontSize: 16 }}>View full roster</Text>
          </TouchableOpacity>
          </View>
          
        </View>
      </View>
    );
  }
}


export class Rosters extends React.Component {

  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    let today = new Date();
    let date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    let dateTime = date + '+' + time;
    // let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/availability_relationships/?worker=9267&ordering=date&date__gte=2019-11-12';
    let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/availability_relationships/?worker=9267&ordering=date&limit=100&format=json';
    const params = {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return fetch(jobUrl, params)
      .then((response) => response.json())
      .then((responseJson) => {

        fetch('https://dimo-testing.biarriworkbench.com/dimo/internal/availability_types/', params)
          .then(res => res.json())
          .then(res => {

            this.setState({
              isLoading: false,
              dataSource: responseJson,
              data: {
                ...this.state.data,
                availability_types: res
              }
            });
          })


      })
      .catch((error) => {
        console.error(error);
      });
  }



  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'stretch' }}>
        <View style={{ flex: 1, padding: 0, alignItems: 'stretch' }}>
          <FlatList
            data={this.state.dataSource.results}
            renderItem={({ item }) => {
              const availability = this.state.data.availability_types.filter(obj => obj.id===item.availability_reason)[0];
              const dt = new Date(item.date)
              const day = dt.getDay()
              return (
                <TouchableOpacity style={styles.gridRow} onPress={() => this.props.navigation.navigate('Info', { item, availability })}>
                  <View style={styles.dateHeader}>
                    <Text style ={{color: '#ffffff'}}>{dayName(day)} - {item.date}</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: 'row', backgroundColor: availability.color }}>
                    <View style={styles.workerJobs}>
                    <Text style={styles.rowText}>Rostered: {availability.name}</Text>
                    <Text style={styles.rowText}>Code: {availability.short_display_name}</Text>
                      <Text style={styles.rowText}>Description: {availability.description}</Text>
                      <Text style={styles.rowText}>Start Time: {availability.start_time}</Text>
                      <Text style={styles.rowText}>End Time: {availability.end_time}</Text>
                    </View>
                    
                  </View>
                </TouchableOpacity>
              );
            }}
            keyExtractor={({ id }, index) => id}
          />

        </View>
        
      </View>
    );
  }
}

export class RosterDetails extends React.Component {



  constructor(props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    let data = this.props.navigation.getParam('item', 'Failed to load job');
    var currentDay = new Date(data.date);
    const nextDay = currentDay.addDays(1);
    let nextDate = nextDay.getFullYear() + '-' + (nextDay.getMonth() + 1) + '-' + nextDay.getDate();
    let jobUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/scheduler_allocations/?worker=9267&ordering=start_time&start_time__gte=' + data.date + '&start_time__lte=' + nextDate;
    return fetch(jobUrl, {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic ' + btoa('ben.friebe@biarri.com:Changeme123'),
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
    })
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function () {

        });

      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {

    if (this, this.state.isLoading) {
      return (
        <View style={{ flex: 1, padding: 20 }}>
          <ActivityIndicator />
        </View>
      )
    }


    let data = this.props.navigation.getParam('item', 'Failed to load roster');
    let availability = this.props.navigation.getParam('availability', 'Failed to load roster');
    const dt = new Date(data.date)
    const day = dt.getDay()
    return (
      
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'stretch' }}>
        
        <View style={{ flex: 1, alignItems: "stretch", backgroundColor: availability.color }}>
          
          <View style={styles.jobInfoHeader}>
            <Text style={styles.jobInfoTitle}>SHIFT: {dayName(day)} - {data.date}</Text>
          </View>
          <View>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Rostered:</Text> {availability.name}</Text>
            {/* <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Status:</Text> {availability.completed ? "Complete" : "Pending"}</Text> */}
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Code:</Text> {availability.short_display_name}</Text>
            {/* <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Start Time:</Text> {data.start_time.substring(11, 16)}</Text> */}
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Description:</Text> {availability.description}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>Start Time:</Text> {availability.start_time}</Text>
            <Text style={styles.jobInfo}><Text style={{fontWeight: 'bold'}}>End Time:</Text> {availability.end_time}</Text>

          </View>
         
        </View>
        <View style={{ flex: 1, padding: 0, alignItems: 'stretch' }}>
        <View style={styles.jobInfoHeader}>
            <Text style={styles.jobInfoTitle}>ALLOCATED JOBS:</Text>
          </View>
          <FlatList
            data={this.state.dataSource}
            renderItem={({ item }) =>
              <TouchableOpacity style={styles.gridRow} onPress={() => this.props.navigation.navigate('Info', { item })}>
                <View style={{ flex: 1, flexDirection: 'row', backgroundColor: item.completed ? "#aaddaa" : "#00bfff" }}>
                  <View style={styles.workerJobs}>
                    <Text style={styles.rowText}>Job ID: {item.id}</Text>
                    <Text style={styles.rowText}>Type: {item.activity_type_name}</Text>
                    <Text style={styles.rowText}>Status: {item.completed ? "Complete" : "Pending"}</Text>
                    <Text style={styles.rowText}>Date: {item.start_date}</Text>
                    <Text style={styles.rowText}>Start Time: {item.start_time.substring(11, 16)}</Text>
                    <Text style={styles.rowText}>Location: {item.location_name}</Text>
                    <Text style={styles.rowText}>Priority: {item.activity_priority_name}</Text>
                  </View>
                  <View style={styles.buttonColumn}>
                    <TouchableOpacity style={styles.jobButton} onPress={() => {
                      Alert.alert("Job Status", 'Job Started!');
                      // this.props.navigation.navigate('Details');
                    }}>
                      <Text style={{ color: '#555555', fontSize: 16 }}>Start</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.jobButton} onPress={() => {
                      Alert.alert("Job Status", 'Job Stopped!');
                      // this.props.navigation.navigate('Details');
                    }}>
                      <Text style={{ color: '#555555', fontSize: 16 }}>Stop</Text>
                    </TouchableOpacity>
                    <Text style={{ color: 'white', paddingTop: 8, fontWeight: 'bold' }}>
                      Allocated Time: {item.allocation_duration}
                    </Text>

                  </View>
                </View>
              </TouchableOpacity>}
            keyExtractor={({ id }, index) => id}
          />

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  workerJobs: {
    padding: 5,
    paddingLeft: 20,
    flex: 1
  },
  workerJobsAll: {
    backgroundColor: '#bb8888',
    height: 100,
    borderTopWidth: 1,
    borderColor: 'white',
    padding: 5,
    paddingLeft: 20
  },
  rowText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#000000'
  },
  header: {
    alignItems: 'center',
    backgroundColor: "#555555",
    padding: 10
  },
  headerText: {
    fontSize: 20,
    color: "#ffffff"
  },
  allButton: {
    backgroundColor: '#888888',
    color: "#ffffff",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    borderRadius: 2,
    margin: 2
  },
  jobButton: {
    backgroundColor: '#ffffff',
    color: "#000000",
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    marginTop: 10,
    width: 150,
    borderRadius: 2
  },
  buttonColumn: {
    padding: 10,
    alignItems: 'center'
  },
  gridRow: {
    borderTopWidth: 1,
    borderColor: 'white',
  },
  jobInfo: {
    fontSize: 20,
    padding: 5,
  },
  jobInfoTitle: {
    fontSize: 25,
    fontWeight: 'bold',
    padding: 5,
    color: "#ffffff"
  },
  jobInfoHeader: {
    backgroundColor: "#555555",
  },
  dateHeader: {
    backgroundColor: '#555555',
    padding: 5
  }
})
