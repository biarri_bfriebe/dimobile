import React, { Component } from 'react';
import {decode as atob, encode as btoa} from 'base-64'
import { Button, Text, View, StyleSheet, ActivityIndicator, Image,
  TouchableOpacity, FlatList, TouchableWithoutFeedback } from 'react-native';
import { Icon } from 'react-native-elements';

// export class Profile extends React.Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <Text style={styles.title}>
//         Profile
//         </Text>
//       </View>
//     );
//   }
// }

export class Profile extends React.Component {

  constructor(props){
    super(props);
    this.state ={ isLoading: true}
  }

  componentDidMount(){
    return fetch('https://dimo-testing.biarriworkbench.com/dimo/internal/scenarios/1/wide_workers/?ordering=name&offset=0&id__in=9267&format=json', {
      method: 'get',
      headers: new Headers({
        'Authorization': 'Basic '+btoa('ben.friebe@biarri.com:Changeme123'), 
        'Content-Type': 'application/x-www-form-urlencoded'
      }),
    })
    .then((response) => response.json())
    .then((responseJson) => {

      this.setState({
        isLoading: false,
        dataSource: responseJson[0],
      }, function(){

      });

    })
    .catch((error) => {
      console.error(error);
    });
  }

  // componentDidMount(){
  //   console.log(this.state.dataSource)
  //   let skillUrl = 'https://dimo-testing.biarriworkbench.com/dimo/internal/skills/?offset=0&limit=50&id__in=' + {dataSource.skills} + '&format=json';
  //   return fetch(skillUrl, {
  //     method: 'get',
  //     headers: new Headers({
  //       'Authorization': 'Basic '+btoa('ben.friebe@biarri.com:Changeme123'), 
  //       'Content-Type': 'application/x-www-form-urlencoded'
  //     }),
  //   })
  //   .then((response) => response.json())
  //   .then((responseJson) => {

  //     this.setState({
  //       isLoading: false,
  //       skillSource: responseJson,
  //     }, function(){

  //     });

  //   })
  //   .catch((error) => {
  //     console.error(error);
  //   });
  // }

  render() {

    if(this,this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={{flex: 1, padding:25}}>
        <View style={{ flex: 1, alignItems: 'center' }}>
            <Image source={require('../assets/profile.png')}
              style={styles.profile} />
        </View>
        <View style={{ flex: 2 }}>
        <Text style={styles.profileRow}>Name: {this.state.dataSource.name}</Text>
        <Text style={styles.profileRow}>ID: {this.state.dataSource.external_id}</Text>
        <Text style={styles.profileRow}>Department: {this.state.dataSource.extra_data.department}</Text>
        <Text style={styles.profileRow}>Manager: {this.state.dataSource.manager}</Text>
        <Text style={styles.profileRow}>Skills: {this.state.dataSource.skills.join()}</Text>
        <Text style={styles.profileRow}>Work Locations: {this.state.dataSource.possible_regions.join()}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  profilePicture: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 5,
    flex: 1
  },
  profileRow: {
    borderTopWidth: 1,
    borderColor: "#555555",
    padding: 15,
    fontSize: 20,
    color: '#555555'
  },
  profile: {
    margin: 20,
    width: 120,
    height: 120,
    resizeMode: 'contain',
    borderRadius: 200
  },
});